const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');

// retorna
router.get('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM gyms;',
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

router.get('/all', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT g.id, g.name, u.name AS "Resp.Quadra", g.score ,g.initial_function_time, g.end_function_time FROM gyms AS g INNER JOIN users AS u ON g.id = u.id;',
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO gyms (name, users_id, is_active, score, initial_function_time, end_function_time ) VALUES (?,?,?,?,?,?)',
            [req.body.name, req.body.users_id, req.body.is_active, req.body.score, req.body.initial_function_time, req.body.end_function_time],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'Gyms cadastrado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM gyms WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE gyms SET name = ?, users_id = ?, is_active = ?, score = ?, initial_function_time = ?, end_function_time = ? WHERE id = ?',
            [req.body.name, req.body.users_id, req.body.is_active, req.body.score, req.body.initial_function_time, req.body.end_function_time, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'Usuario alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

module.exports = router;