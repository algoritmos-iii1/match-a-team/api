const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');

// retorna
router.get('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT s.id, u.name AS responsavel, s.created_by, s.type FROM schedules AS s INNER JOIN users AS u ON s.id = u.id;',
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO schedules (users_id, created_by, type) VALUES (?,?,?)',
            [req.body.users_id, req.body.created_by, req.body.type],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'schedules agendada com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM schedules WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE schedules SET user_id = ?, created_by = ?, type = ? WHERE id = ?',
            [req.body.date_user_id, req.body.created_by, req.body.type, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'schedules alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });

    router.delete('/', login, (req, res, next) => {
        mysql.getConnection((error, conn) => {
            if (error) { return res.status(500).send({ error: error }) }
            conn.query(
                'DELETE FROM schedules WHERE id = ?;',
                [req.body.id],
                (error, resultado, field) => {
                    conn.release();
                    if (error) { return res.status(500).send({ error: error }) }
                    res.status(202).send({
                        mensagem: 'Schedules Excluido com Sucesso'
                    });
                }
            )
        });
    });

});



module.exports = router;