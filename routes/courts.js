const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');

// retorna
router.get('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM courts',
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO courts (member_max, member_min, gyms_id, schedules_id) VALUES (?,?,?,?)',
            [req.body.member_max, req.body.member_min, req.body.gyms_id, req.body.schedules_id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'courts agendada com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM courts WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE schedules SET user_id = ?, created_by = ?, type = ? WHERE id = ?',
            [req.body.date_user_id, req.body.created_by, req.body.type, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'courts alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });

    router.delete('/', login, (req, res, next) => {
        mysql.getConnection((error, conn) => {
            if (error) { return res.status(500).send({ error: error }) }
            conn.query(
                'DELETE FROM courts WHERE id = ?;',
                [req.body.id],
                (error, resultado, field) => {
                    conn.release();
                    if (error) { return res.status(500).send({ error: error }) }
                    res.status(202).send({
                        mensagem: 'courts Excluido com Sucesso'
                    });
                }
            )
        });
    });

});



module.exports = router;