const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');

// retorna
router.get('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM adresses;',
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO adresses (cep, name, number, city, state, country, users_id) VALUES (?,?,?,?,?,?,?)',
            [req.body.cep, req.body.name, req.body.number, req.body.city, req.body.state, req.body.country, req.body.users_id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'adresses ok',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM adresses WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});
// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE adresses SET cep = ?, name = ?, number = ?, city = ?, state = ?, country = ?, users_id = ? WHERE id = ?',
            [req.body.cep, req.body.name, req.body.number, req.body.city, req.body.state, req.body.country, req.body.users_id, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'adresses alterado',
                    id: resultado.insertId
                });
            }
        )
    });

    router.delete('/', login, (req, res, next) => {
        mysql.getConnection((error, conn) => {
            if (error) { return res.status(500).send({ error: error }) }
            conn.query(
                'DELETE FROM adresses WHERE id = ?;',
                [req.body.id],
                (error, resultado, field) => {
                    conn.release();
                    if (error) { return res.status(500).send({ error: error }) }
                    res.status(202).send({
                        mensagem: 'adresses excluido'
                    });
                }
            )
        });
    });

});



module.exports = router;