const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');


// retorna
router.get('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM Matchs;',
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// retorna
router.get('/trat/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT m.id, (g.name) as gyms, (c.id) as courts_id, m.dataPart, m.horarioPart, m.tempoJogo, m.start_time, m.end_time, m.name, m.type, m.description, (u.username) AS responsible FROM Matchs AS m INNER JOIN users AS u ON (m.responsible_id = u.id) INNER JOIN courts AS c ON (m.courts_id = c.id) INNER JOIN gyms AS g ON (m.gyms_id = g.id);',
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

router.get('/trata/:id', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT m.id, (g.name) as gyms, (c.id) as courts_id, m.start_time, m.dataPart, m.horarioPart, m.tempoJogo, m.end_time, m.name, m.type, m.description, (u.username) AS responsible FROM Matchs AS m INNER JOIN users AS u ON (m.responsible_id = u.id) INNER JOIN courts AS c ON (m.courts_id = c.id) INNER JOIN gyms AS g ON (m.gyms_id = g.id) WHERE m.id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

router.get('/responsible/:id', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT m.id, (g.name) as gyms, (c.id) as courts_id, m.start_time, m.dataPart, m.horarioPart, m.tempoJogo, m.end_time, m.name, m.type, m.description, (u.username) AS responsible FROM Matchs AS m INNER JOIN users AS u ON (m.responsible_id = u.id) INNER JOIN courts AS c ON (m.courts_id = c.id) INNER JOIN gyms AS g ON (m.gyms_id = g.id) WHERE m.responsible_id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});


// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO Matchs (gyms_id, courts_id, start_time, end_time, name, type, description, responsible_id, dataPart, horarioPart, tempoJogo) VALUES (?,?,?,?,?,?,?,?,?,?,?)',
            [req.body.gyms_id, req.body.courts_id, req.body.start_time, req.body.end_time, req.body.name, req.body.type, req.body.description, req.body.responsible_id, req.body.dataPart, req.body.dataPart, req.body.horarioPart, req.body.tempoJogo],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'match criado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/id/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM Matchs WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

router.post('/inmatch', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM Matchs_has_users WHERE Matchs_id = ? AND users_id = ?',
            [req.body.Matchs_id, req.body.users_id],
            (error, resultado, field) => {
                if (error) { return res.status(500).send({ error: error }) }
                if (resultado.length > 0) {
                    conn.query(
                        'DELETE FROM Matchs_has_users WHERE Matchs_id = ? AND users_id = ?;',
                        [req.body.Matchs_id, req.body.users_id],
                        (error, resultado, field) => {
                            conn.release();
                            if (error) { return res.status(500).send({ error: error }) }
                            res.status(201).send({
                                mensagem: 'users exit',
                            });
                        }
                        )
                
                } else {
                    conn.query(
                        'INSERT INTO Matchs_has_users (Matchs_id, users_id) VALUES (?,?)',
                        [req.body.Matchs_id, req.body.users_id],
                        (error, resultado, field) => {
                            conn.release();
                            if (error) { return res.status(500).send({ error: error }) }
                            res.status(201).send({
                                mensagem: 'OK',
                            });
                        }
                    )
                }
            });
    });
});
//
router.get('/findmatch/:id', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT Matchs_id, (u.username) AS username FROM Matchs_has_users AS mu INNER JOIN users AS u ON (mu.users_id = u.id) WHERE Matchs_id = ?',
            [req.params.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE Matchs SET gyms_id = ?, courts_id = ?, start_time = ?, end_time = ?, name = ?, type = ?, description = ?, responsible_id = ? WHERE id = ?',
            [req.body.gyms_id, req.body.courts_id, req.body.start_time, req.body.end_time, req.body.name, req.body.type, req.body.description, req.body.responsible_id, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'matchs alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });

    router.delete('/', login, (req, res, next) => {
        mysql.getConnection((error, conn) => {
            if (error) { return res.status(500).send({ error: error }) }
            conn.query(
                'DELETE FROM Matchs WHERE id = ?;',
                [req.body.id],
                (error, resultado, field) => {
                    conn.release();
                    if (error) { return res.status(500).send({ error: error }) }
                    res.status(202).send({
                        mensagem: 'matchs Excluido com Sucesso'
                    });
                }
            )
        });
    });

});



module.exports = router;